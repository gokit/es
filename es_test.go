package es

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSubList(t *testing.T) {
	stream := New()
	defer stream.Close()

	var list SubList

	seenChan := make(chan interface{}, 5)
	list.Add(stream.Subscribe(func(i interface{}) { seenChan <- i }))
	list.Add(stream.Subscribe(func(i interface{}) { seenChan <- i }))
	list.Add(stream.Subscribe(func(i interface{}) { seenChan <- i }))
	list.Add(stream.Subscribe(func(i interface{}) { seenChan <- i }))
	list.Add(stream.Subscribe(func(i interface{}) { seenChan <- i }))

	assert.Len(t, list, 5)

	assert.Len(t, seenChan, 0)
	stream.Publish(2)
	assert.Len(t, seenChan, 5)

	for i := 0; i < 5; i++ {
		<-seenChan
	}

	list.WithPredicate(func(i interface{}) bool {
		if i == 2 {
			return false
		}
		return true
	})

	stream.Publish(2)
	assert.Len(t, seenChan, 0)
	assert.Len(t, list, 5)
	list.Stop()

	stream.Publish(1)
	assert.Len(t, seenChan, 0)
}

func TestEventStream(t *testing.T) {
	stream := New()
	defer stream.Close()

	seenChan := make(chan int, 1)
	sub := stream.Subscribe(func(i interface{}) {
		seenChan <- 1
	})

	stream.Publish(1)
	assert.Equal(t, <-seenChan, 1)
	sub.Stop()
	stream.Publish(1)
	assert.Equal(t, len(seenChan), 0)
}

func TestEventStream_Close(t *testing.T) {
	stream := New()

	seenChan := make(chan int, 1)
	stream.Subscribe(func(i interface{}) {
		seenChan <- 1
	})

	stream.Close()
	stream.Publish(1)
	assert.Equal(t, len(seenChan), 0)
}

func TestEventStream_Predicate(t *testing.T) {
	stream := New()
	defer stream.Close()

	seenChan := make(chan int, 1)
	sub := stream.Subscribe(func(i interface{}) {
		seenChan <- 1
	})

	defer sub.WithPredicate(func(i interface{}) bool {
		if i == 2 {
			return false
		}
		return true
	}).Stop()

	stream.Publish(1)
	assert.Equal(t, <-seenChan, 1)
	stream.Publish(2)
	assert.Equal(t, len(seenChan), 0)
}
